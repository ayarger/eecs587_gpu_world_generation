This is an experiment in gpu-based procedural world generation. Created by Austin Yarger for Professor Quentin Stout's EECS 587: Parallel Computing Course at the University of Michigan, Fall 2015.

// usage: ./cuda_game <world_width (int)> <world_height (int)> <room_width (int)> <room_height (int)> <test_cpu_too (true/false)> <num_runs (int)> <disable_simulation (true/false)> <verbose_output (true/false)>

Credits
Royalty-Free art, including the water texture and rock images, obtained from www.opengameart.org.

The Simple DirectMedia Layer (SDL) was used in the interactive portion of this program. SDL is available at www.libsdl.org.

The SDL application code written for the interactive portion of the program was initially based off of tutorials by Lazy Foo Productions. Visit Lazy Foo Productions at www.lazyfoo.net.

Note:
This project utilizes the following dependencies...
-SDL2
-SDL2_image
-CUDA Framework
You will need to obtain these dependencies, and likely adjust the makefile to point to them. Then, simply "make".