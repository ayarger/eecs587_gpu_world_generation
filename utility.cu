#include "utility.h"

#include <iostream>

std::unordered_map<std::string, time_point> utility::_time_points;
std::mutex utility::print_mutex;
bool utility::global_verbose = false;

void utility::start_clock(const std::string & s)
{
	time_point start_time = std::chrono::high_resolution_clock::now();
	_time_points[s] = start_time;
}

void utility::print_clock(const std::string & s)
{
	time_point current_time = std::chrono::high_resolution_clock::now();

	utility::print_mutex.lock();
	std::cout << "Wall clock time passed for clock [" << s << "]:\n"
			  << std::chrono::duration<double, std::milli>(current_time - _time_points[s]).count()
			  << " ms\n\n";
	utility::print_mutex.unlock();
}

