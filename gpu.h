#pragma once

#include <mutex>
#include <curand_kernel.h>

class gpu
{
public:
    static int world_width;
    static int world_height;
    static int room_width;
    static int room_height;
    // Should we perform cpu too, or just the gpu?
    static bool test_both_gpu_and_cpu;
    // Num runs to perform for both gpu and cpu.
    static int num_runs;
    // Don't bother visualizing the data.
    static bool dry_run;
    static int* host_map;
    static int** device_map;
    static std::mutex map_mutex;
    static curandState* devStates;

    static void init(bool verbose);
    static void exp_init(bool verbose, bool gen_rand);
    static void thread_based_init();
    static void print_map_status();
    static void* allocate_device_mem(int bytes);
    static int access_host_map(int x, int y);
};
