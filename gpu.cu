#include "gpu.h"

#include <curand_kernel.h>

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <time.h>
#include <vector>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <thread>
#include "utility.h"

int gpu::world_width = 8;
int gpu::world_height = 8;
int gpu::room_width = 8;
int gpu::room_height = 8;
bool gpu::test_both_gpu_and_cpu = false;
int gpu::num_runs = 1;
bool gpu::dry_run = false;
int* gpu::host_map;
int** gpu::device_map;
curandState* gpu::devStates;

std::mutex gpu::map_mutex;

void DisplayHeader()
{
    const int kb = 1024;
    const int mb = kb * kb;
    std::cout << "NBody.GPU" << std::endl << "=========" << std::endl << std::endl;

    std::cout << "CUDA version:   v" << CUDART_VERSION << std::endl;    

    int devCount;
    cudaGetDeviceCount(&devCount);
    std::cout << "CUDA Devices: " << std::endl << std::endl;

    for(int i = 0; i < devCount; ++i)
    {
        cudaDeviceProp props;
        cudaGetDeviceProperties(&props, i);
        std::cout << i << ": " << props.name << ": " << props.major << "." << props.minor << std::endl;
        std::cout << "  Global memory:   " << props.totalGlobalMem / mb << "mb" << std::endl;
        std::cout << "  Shared memory:   " << props.sharedMemPerBlock / kb << "kb" << std::endl;
        std::cout << "  Constant memory: " << props.totalConstMem / kb << "kb" << std::endl;
        std::cout << "  Block registers: " << props.regsPerBlock << std::endl << std::endl;

        std::cout << "  Warp size:         " << props.warpSize << std::endl;
        std::cout << "  Threads per block: " << props.maxThreadsPerBlock << std::endl;
        std::cout << "  Max block dimensions: [ " << props.maxThreadsDim[0] << ", " << props.maxThreadsDim[1]  << ", " << props.maxThreadsDim[2] << " ]" << std::endl;
        std::cout << "  Max grid dimensions:  [ " << props.maxGridSize[0] << ", " << props.maxGridSize[1]  << ", " << props.maxGridSize[2] << " ]" << std::endl;
        std::cout << std::endl;
    }
}

#define gpuErrchk(ans) {gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
    if(code != cudaSuccess)
    {
        fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if(abort)
            exit(code);
    }
}

__device__ void build_river(int* devPtr, const coord & starting_coord, const coord & ending_coord, const session_settings & ses_settings, const coord & origin)
{   
    return;
    //printf("scratch array size: %d", scratch_array_size);
    coord current_coord = starting_coord;

    int num = 0;
    while(true)
    {
        // Transform cell into water.
        devPtr[origin.x + current_coord.x + (origin.y + current_coord.y) * ses_settings.world_width] = 2;

        int delta_x = ending_coord.x - current_coord.x;
        int delta_y = ending_coord.y - current_coord.y;
        if(delta_x > 1)
            delta_x = 1;
        else if(delta_x < -1)
            delta_x = -1;

        if(delta_y > 1)
            delta_y = 1;
        else if(delta_y < -1)
            delta_y = -1;

        if(delta_x == 0 && delta_y == 0)
            break;

        current_coord = coord(current_coord.x + delta_x, current_coord.y + delta_y);
    }

    // Tree stuff.
    //int tree_position_x = ses_settings.room_width * curand_uniform(&rand);
    //int tree_position_y = ses_settings.room_height * curand_uniform(&rand);
    //devPtr[tree_position_x + tree_position_y * ses_settings.room_width] = 3;
}

__global__ void river_build_kernel(int* devPtr, coord* river_points_array, curandState* rand_state, int world_width, int world_height, int room_width, int room_height)
{
    return;
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    curandState rand_thread_state = rand_state[index];
    session_settings ses_settings(world_width, world_height, room_width, room_height);
    int num_regions_wide = ses_settings.world_width / ses_settings.room_width;
    int num_regions_tall = ses_settings.world_height / ses_settings.room_height;

    coord begin_river_point = river_points_array[blockIdx.x * 2]; 
    coord end_river_point = river_points_array[blockIdx.x * 2 + 1];

    coord* from_south_point = river_points_array + blockIdx.x * 4; 
    coord* from_west_point = from_south_point + 1;
    coord* north_point = from_south_point + 2;
    coord* east_point = from_south_point + 3;

    coord origin((index % num_regions_wide) * ses_settings.room_width, (index / num_regions_wide) * ses_settings.room_height);

    //build_river(devPtr, coord(0, 0), coord(room_width-1, room_height-1), ses_settings, origin);

    // Apply river points (testing)
    
    bool not_on_south_border = (blockIdx.x / num_regions_wide < num_regions_tall-1);
    bool not_on_west_border = (blockIdx.x % num_regions_wide != 0);
    bool not_on_north_border = (blockIdx.x / num_regions_wide >= 1);
    bool not_on_east_border = (blockIdx.x+1 % num_regions_wide != 0);

    if(not_on_north_border && not_on_east_border && not_on_west_border && not_on_south_border)
    {
        // connect from_west to north point.
        float r = curand_uniform(rand_state);
        //if(r > 0.833)
            build_river(devPtr, *from_west_point, *north_point, ses_settings, origin);
        //else if(r > 0.66)
            build_river(devPtr, *from_west_point, *east_point, ses_settings, origin);
        //else if(r > 0.5)
            build_river(devPtr, *from_west_point, *from_south_point, ses_settings, origin);
        //else if(r > 0.33)
            build_river(devPtr, *from_south_point, *east_point, ses_settings, origin);
        //else if(r > 0.16)
            build_river(devPtr, *from_south_point, *north_point, ses_settings, origin);
        //else
            build_river(devPtr, *east_point, *north_point, ses_settings, origin);
    }

    /*
    if(blockIdx.x / num_regions_wide < num_regions_tall-1)
    {
        printf("from_south_point->x:%d\n", from_south_point->x);
        printf("from_south_point->y:%d\n", from_south_point->y);
    }

    if(blockIdx.x % num_regions_wide != 0)
    {
        printf("from_west_point->x:%d\n", from_west_point->x);
        printf("from_west_point->y:%d\n", from_west_point->y);
    }

    if(blockIdx.x / num_regions_wide >= 1)
    {
        printf("north_point->x:%d\n", north_point->x);
        printf("north_point->y:%d\n", north_point->y);
    }

    if(blockIdx.x+1 % num_regions_wide != 0)
    {
        printf("east_point->x:%d\n", east_point->x);
        printf("east_point->y:%d\n", east_point->y);
    }  
    */
    //devPtr[origin.x + from_south_point->x + (origin.y + from_south_point->y) * ses_settings.world_width] = 9;

}

__global__ void river_preparation_kernel(int* devPtr, coord* river_points_array, curandState* rand_state, int world_width, int world_height, int room_width, int room_height)
{
    //build_river
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    curandState rand_thread_state = rand_state[index];
    session_settings ses_settings(world_width, world_height, room_width, room_height);
    int num_regions_wide = ses_settings.world_width / ses_settings.room_width;

    coord* from_south_point = river_points_array + blockIdx.x * 4; 
    coord* from_west_point = from_south_point + 1;
    coord* north_point = from_south_point + 2;
    coord* east_point = from_south_point + 3;

    //printf("blockIdx.x:%d, blockIdx.x - gridDim.x:%d\n", blockIdx.x, (blockIdx.x - num_regions_wide));

    coord* block_north_of_us_point = river_points_array + (blockIdx.x - num_regions_wide) * 4;
    coord* block_east_of_us_point = river_points_array + (blockIdx.x + 1) * 4 + 1; // May be misaligned here.

    // Communicate Northward.
    //printf("num_regions_wide:%d\n", num_regions_wide);
    //printf("I am block:%d and should I push north:%d\n", blockIdx.x, (blockIdx.x / num_regions_wide >= 1));
    //printf("PUSH NORTH\n");

    float r = curand_uniform(rand_state);
    if(blockIdx.x / num_regions_wide >= 1)
    {

        *block_north_of_us_point = coord(1+(r*room_width-1), room_height-1);
        *north_point = coord(1+(r*room_width-1), 0);
    }

    //printf("PUSH EAST\n");
    r = curand_uniform(rand_state);
    //Communicate Eastward.
    if(blockIdx.x+1 % num_regions_wide != 0)
    {
        *block_east_of_us_point = coord(0, 1+(r*room_height-1));
        *east_point = coord(room_width-1, 1+(r*room_height-1));
    }

    // Push corresponding coords to us.

    //coord origin((index % num_regions_wide) * ses_settings.room_width, (index / num_regions_wide) * ses_settings.room_height);
}

__device__ __host__ int num_visited_adjacent_spaces(const int* devPtr, const coord & co, const session_settings & ses_settings)
{
    int num_visited_adjacent_spaces = 0;
    
    num_visited_adjacent_spaces += (co.y > 0 && devPtr[co.x + (co.y-1)*ses_settings.room_width] == 0);
    num_visited_adjacent_spaces += (co.x < ses_settings.room_width-1 && devPtr[(co.x+1) + co.y*ses_settings.room_width] == 0);
    num_visited_adjacent_spaces += (co.y < ses_settings.room_height-1 && devPtr[co.x + (co.y+1)*ses_settings.room_width] == 0);
    num_visited_adjacent_spaces += (co.x > 0 && devPtr[(co.x-1) + co.y*ses_settings.room_width] == 0);

    return num_visited_adjacent_spaces;
}

__device__ bool add_potential_coords(const int* devPtr, ay_stack & stack, const coord & co, curandState & rand, const session_settings & ses_settings)
{
    int adjacency_limit = 1;

    //NORTH
    if(co.y > 0)
    {
    	if(devPtr[co.x + (co.y-1)*ses_settings.room_width] == 1)
    	{
    	    if(num_visited_adjacent_spaces(devPtr, coord(co.x, co.y-1), ses_settings) <= adjacency_limit)
    		stack.push(coord(co.x, co.y-1));
    	}	
    }
    //EAST
    if(co.x < ses_settings.room_width-1)
    {
    	if(devPtr[(co.x+1) + co.y*ses_settings.room_width] == 1)
    	{
    	    if(num_visited_adjacent_spaces(devPtr, coord(co.x+1, co.y), ses_settings) <= adjacency_limit)
    		stack.push(coord(co.x+1, co.y));
    	}
    }
    //SOUTH
    if(co.y < ses_settings.room_height-1)
    {
    	if(devPtr[co.x + (co.y+1)*ses_settings.room_width] == 1)
    	{
    	    if(num_visited_adjacent_spaces(devPtr, coord(co.x, co.y+1), ses_settings) <= adjacency_limit)
    		stack.push(coord(co.x, co.y+1));
    	}
    }
    //WEST TODO:.
    if(co.x > 0)
    {
    	if(devPtr[(co.x-1) + co.y*ses_settings.room_width] == 1)
    	{
    	    if(num_visited_adjacent_spaces(devPtr, coord(co.x-1, co.y), ses_settings) <= adjacency_limit)
    		stack.push(coord(co.x-1, co.y));
    	}
    }

    stack.shuffle(rand);

    return false;
}

 __device__ void search_room(int* devPtr, curandState & rand, const session_settings & ses_settings)
{   
    //printf("scratch array size: %d", scratch_array_size);
    __shared__ coord shared_stack_space[(int)(8*8)];
    ay_stack stack(shared_stack_space, 8*8);
    stack.push(coord(0, 0));

    int num = 0;
    while(!stack.empty())
    {
        //printf("\nWITHIN STACK\n");

    	// Grab next coord.
    	coord current_coord = stack.top();
    	stack.pop();
    	
    	if(num_visited_adjacent_spaces(devPtr, coord(current_coord.x, current_coord.y), ses_settings) > 1)
    	    continue;

    	devPtr[current_coord.x + current_coord.y*ses_settings.room_width] = 0;

    	num ++;
    		
    	add_potential_coords(devPtr, stack, current_coord, rand, ses_settings);
    	
    	stack.to_string();
    }

    // Tree stuff.
    //int tree_position_x = ses_settings.room_width * curand_uniform(&rand);
    //int tree_position_y = ses_settings.room_height * curand_uniform(&rand);
    //devPtr[tree_position_x + tree_position_y * ses_settings.room_width] = 3;
}

__device__ __host__ void clear_room(int * devPtr, const session_settings & ses_settings)
{
    for(int x = 0; x < ses_settings.room_width; x++)
    {
    	for(int y = 0; y < ses_settings.room_height; y++)
    	{
    	    devPtr[x + y * ses_settings.room_width] = 1;
    	}
    }
}

/*extern __shared__ coord personal_scratch_array[];
__device__ coord* get_shared_personal_scratch_array()
{
    return (coord*)personal_scratch_array;
}*/

__global__ void carving_kernel(int* devPtr, curandState* rand_state, int world_width, int world_height, int room_width, int room_height)
{
    session_settings ses_settings(world_width, world_height, room_width, room_height);
    
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    //printf("index: %d\n", index);
    //printf("scratch size per thread: %d", scratch_space_per_thread);
    curandState rand_thread_state = rand_state[index];

    int num_regions_wide = ses_settings.world_width / ses_settings.room_width;
    int num_regions_tall = ses_settings.world_height / ses_settings.room_height;

    coord origin((index % num_regions_wide) * ses_settings.room_width, (index / num_regions_wide) * ses_settings.room_height);

    // Attempt at static shared memory.
    //int shared_space_required = sizeof(int) * ses_settings.room_width * ses_settings.room_height;
    __shared__ int dev_ptr_shared[8*8];


    // Prepare room.
    clear_room(dev_ptr_shared, ses_settings);

    // Construct river.
    //build_river(dev_ptr_shared, rand_thread_state, ses_settings);

    // Search room.
    search_room(dev_ptr_shared, rand_thread_state, ses_settings);

    __syncthreads();

    // Move block mem to global mem.
    for(int x = 0; x < room_width; x++) // TODO: investigate the amount of scratch mem we're giving to each block. I think we're giving FAR too much, due to the fact that we switched from threads to blocks.
    {
        for(int y = 0; y < room_height; y++)
        {
            //dev_ptr_shared[x + room_width * y] = devPtr[origin.x + x + world_width * (origin.y + y)];
            devPtr[origin.x + x + (origin.y + y) * world_width] = dev_ptr_shared[x + y * room_width];
            //devPtr[origin.x + x + (origin.y + y) * world_width] = index;
        }
    }
}

// Thanks to aresio.blogspot.com
__global__ void setup_kernel(curandState* state, unsigned long seed)
{
    int id = threadIdx.x + blockIdx.x;
    curand_init(seed, id, 0, &state[id]);
}

__global__ void uva_test(int* int_arr, char* char_arr, int n)
{
    printf("uva_test!");
    return;
    for(int i = 0; i < n; i++)
    {
        printf("int:%d, char:%c\n", int_arr[i], char_arr[i]);
    }
    printf("uva_test concluded!");
}

__global__ void wander_kernel(int* devPtr, coord* free_cells_array, int* num_free_cells_array, int world_width, int world_height, int room_width, int room_height)
{
    __shared__ int dev_ptr_shared[32*32];

    __shared__ coord free_cells_array_shared[10];
    __shared__ int num_free_cells_shared;
    num_free_cells_shared = num_free_cells_array[blockIdx.x];
    free_cells_array = free_cells_array + blockIdx.x * 10;
    
    for(int i = 0; i < num_free_cells_shared; i++)
        free_cells_array_shared[i] = free_cells_array[i];
    

    if(num_free_cells_shared < 2)
        return;

    session_settings ses_settings(world_width, world_height, room_width, room_height);
    
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    //printf("index: %d\n", index);
    //printf("scratch size per thread: %d", scratch_space_per_thread);

    int num_regions_wide = ses_settings.world_width / ses_settings.room_width;
    int num_regions_tall = ses_settings.world_height / ses_settings.room_height;

    coord origin((index % num_regions_wide) * ses_settings.room_width, (index / num_regions_wide) * ses_settings.room_height);

    // Attempt at static shared memory.
    //int shared_space_required = sizeof(int) * ses_settings.room_width * ses_settings.room_height;
    


    // Prepare room.
    //clear_room(dev_ptr_shared, ses_settings);

    //dev_ptr_shared[threadIdx.x] = blockIdx.x;

    // Construct river.
    //build_river(dev_ptr_shared, rand_thread_state, ses_settings);

    // Search room.
    //search_room(dev_ptr_shared, rand_thread_state, ses_settings);

    //printf("hi from index:%d\n", index);

    //devPtr[index] = 0;
    int room_x = threadIdx.x % room_width;
    int room_y = threadIdx.x / room_width;
    int world_x = index % world_width;
    int world_y = index / world_width;

    dev_ptr_shared[room_x + room_width * room_y] = 1;
    
    //printf("%\n", b);
    //dev_ptr_shared[room_x + room_width * room_y] = (int)!b;

    for(int i = 0; i < num_free_cells_shared-1; i++)
    {
        coord c1 = free_cells_array_shared[i];
        coord c2 = free_cells_array_shared[i+1];
        coord my_coord(room_x, room_y);

        if(c1.x == c2.x && c1.y == c2.y)
            continue;

        float dist = fabsf((c2.y - c1.y) * room_x - (c2.x - c1.x) * room_y + c2.x * c1.y - c2.y * c1.x) / sqrtf(powf(c2.y - c1.y, 2) + powf(c2.x - c1.x, 2));
        float dist_from_point_1 = fabsf(sqrtf(pow(c1.x - room_x, 2) + powf(c1.y - room_y, 2)));
        float dist_from_point_2 = fabsf(sqrtf(pow(c2.x - room_x, 2) + powf(c2.y - room_y, 2)));
        float dist_between_points = fabsf(sqrtf(pow(c1.x - c2.x, 2) + powf(c1.y - c2.y, 2)));
        //bool b = (dist <= 1.41421356237);
        bool b = dist <= 1.0 && dist_from_point_2 <= dist_between_points && dist_from_point_1 <= dist_between_points;
        //dev_ptr_shared[room_x + room_width * room_y] = (int)!b;

        if(b || dev_ptr_shared[room_x + room_width * room_y] == 0)
            dev_ptr_shared[room_x + room_width * room_y] = 0;
    }

    //if(b)
    //    atomicAdd(&free_cell_counter, 1);

    devPtr[world_x + world_width * (world_y)] = dev_ptr_shared[room_x + room_width * room_y];
}

__global__ void wander_preparation_kernel(coord* free_cells_array, int* num_free_cells_array, curandState* rand_state, int room_width, int room_height)
{
    //build_river
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    curandState rand_thread_state = rand_state[index];
    
    free_cells_array = free_cells_array + blockIdx.x * 10; // 10 free cells max.
    num_free_cells_array = num_free_cells_array + blockIdx.x;

    //int num_free_cells = curand_uniform(&rand_thread_state) * 10;
    int num_free_cells = 5;
    *num_free_cells_array = num_free_cells;

    //printf("num free cells:%d\n", num_free_cells);
    for(int i = 0; i < num_free_cells; i++)
    {
        int rand_x = curand_uniform(&rand_thread_state) * room_width;
        int rand_y = curand_uniform(&rand_thread_state) * room_height;

        free_cells_array[i] = coord(rand_x, rand_y);
    }
}

void gpu::exp_init(bool verbose, bool gen_rand)
{
    srand(time(NULL));
    session_settings ses_settings(gpu::world_width, gpu::world_height, gpu::room_width, gpu::room_height);

    int num_blocks = (ses_settings.world_width / ses_settings.room_width) * (ses_settings.world_height / ses_settings.room_height);
    int stack_space_required = ses_settings.room_width * ses_settings.room_height * num_blocks; // RUN CUDA DEBUGGER TO DISCOVER WHY CRASH HAPPENS WHEN WE DO WORLD:256, ROOM:4.
    
    gpu::host_map = new int[ses_settings.world_width * ses_settings.world_height]();
    int* devPtr;
    coord* scratch_array;
    coord* river_points_array;
    coord* free_cells_array;
    int* num_free_cells_array;

    if(gen_rand)
    {
        cudaMalloc(&gpu::devStates, num_blocks * sizeof(curandState));
        setup_kernel<<<1, 256>>> (gpu::devStates, time(NULL));
    }
    
    // PIN WAY
    //cudaHostAlloc((void**)&devPtr, sizeof(int) * ses_settings.world_width * ses_settings.world_height, cudaHostAllocMapped);
    //cudaHostAlloc((void**)&scratch_array, sizeof(coord) * stack_space_required, cudaHostAllocMapped);

    // OLD WAY
    cudaMalloc((void**)&devPtr, sizeof(int) * ses_settings.world_width * ses_settings.world_height);
    cudaMalloc((void**)&river_points_array, sizeof(coord) * 4 * num_blocks);
    cudaMalloc((void**)&free_cells_array, sizeof(coord) * 10 * num_blocks);
    cudaMalloc((void**)&num_free_cells_array, sizeof(int) * num_blocks);

    //session_settings* ses_settings_ptr;
    //cudaMalloc((void**)&ses_settings_ptr, sizeof(ses_settings));

    

    //printf("SHIT\nwoorld width:%i, height:%i, room_width:%i, room_height:%i\n", ses_settings.world_width, ses_settings.world_height, ses_settings.room_width, ses_settings.room_height);
    int shared_space_required = sizeof(int) * ses_settings.room_width * ses_settings.room_height;


    //printf("stack_space_required: %d\n", stack_space_required);
    //printf("num_blocks:%d\n", num_blocks);

    wander_preparation_kernel<<<num_blocks, 1>>>(free_cells_array, num_free_cells_array, gpu::devStates, ses_settings.room_width, ses_settings.room_height);
    wander_kernel<<<num_blocks, 32*32>>>(devPtr, free_cells_array, num_free_cells_array, ses_settings.world_width, ses_settings.world_height, ses_settings.room_width, ses_settings.room_height);
    //river_preparation_kernel<<<num_blocks, 1>>>(devPtr, river_points_array, devStates, ses_settings.world_width, ses_settings.world_height, ses_settings.room_width, ses_settings.room_height);
    //river_build_kernel<<<num_blocks, 1>>>(devPtr, river_points_array, devStates, ses_settings.world_width, ses_settings.world_height, ses_settings.room_width, ses_settings.room_height);

    //printf("PRE Kernel Finish\n");
    //printf("\nERRORS:\n");
    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    //printf("Kernel Finish\n");

    gpu::map_mutex.lock();
    //memcpy(gpu::host_map, devPtr, ses_settings.world_width * ses_settings.world_height * sizeof(int));
    cudaMemcpy(gpu::host_map, devPtr, sizeof(int)*ses_settings.world_width*ses_settings.world_height, cudaMemcpyDeviceToHost);

    gpu::map_mutex.unlock();
    //std::cout << "PRINTING WORLD" << std::endl;
    
    if(!verbose)
        return;

    for(int i = 0; i < ses_settings.world_width*ses_settings.world_height; i++)
    {
        std::cout << gpu::host_map[i] << ' ';
        if((i+1) % ses_settings.world_width == 0)
            std::cout << '\n';
    }
}

void gpu::init(bool verbose)
{
    //int N = 10;
    //int* int_arr;
    //char* char_arr;

    //std::cout << "PRE-LAUNCH!" << std::endl;

    //uva_test<<<1, 1>>>(int_arr, char_arr, N);
    //std::cout << "FINISHED!" << std::endl;
    srand(time(NULL));
    session_settings ses_settings(gpu::world_width, gpu::world_height, gpu::room_width, gpu::room_height);

    int num_blocks = (ses_settings.world_width / ses_settings.room_width) * (ses_settings.world_height / ses_settings.room_height);
    int stack_space_required = ses_settings.room_width * ses_settings.room_height * num_blocks; // RUN CUDA DEBUGGER TO DISCOVER WHY CRASH HAPPENS WHEN WE DO WORLD:256, ROOM:4.
    
    gpu::host_map = new int[ses_settings.world_width * ses_settings.world_height]();
    curandState* devStates;
    int* devPtr;
    coord* scratch_array;
    coord* river_points_array;

    cudaMalloc(&devStates, num_blocks * sizeof(curandState));
    
    // PIN WAY
    //cudaHostAlloc((void**)&devPtr, sizeof(int) * ses_settings.world_width * ses_settings.world_height, cudaHostAllocMapped);
    //cudaHostAlloc((void**)&scratch_array, sizeof(coord) * stack_space_required, cudaHostAllocMapped);

    // OLD WAY
    cudaMalloc((void**)&devPtr, sizeof(int) * ses_settings.world_width * ses_settings.world_height);
    cudaMalloc((void**)&river_points_array, sizeof(coord) * 4 * num_blocks);
    //session_settings* ses_settings_ptr;
    //cudaMalloc((void**)&ses_settings_ptr, sizeof(ses_settings));

    //setup_kernel<<<1, num_threads>>> (devStates, time(NULL));

    //printf("SHIT\nwoorld width:%i, height:%i, room_width:%i, room_height:%i\n", ses_settings.world_width, ses_settings.world_height, ses_settings.room_width, ses_settings.room_height);
    int shared_space_required = sizeof(int) * ses_settings.room_width * ses_settings.room_height;

    printf("stack_space_required: %d\n", stack_space_required);
    printf("num_blocks:%d\n", num_blocks);
    //carving_kernel<<<num_blocks, 1>>>(devPtr, devStates, ses_settings.world_width, ses_settings.world_height, ses_settings.room_width, ses_settings.room_height);
    river_preparation_kernel<<<num_blocks, 1>>>(devPtr, river_points_array, devStates, ses_settings.world_width, ses_settings.world_height, ses_settings.room_width, ses_settings.room_height);
    river_build_kernel<<<num_blocks, 1>>>(devPtr, river_points_array, devStates, ses_settings.world_width, ses_settings.world_height, ses_settings.room_width, ses_settings.room_height);

    printf("PRE Kernel Finish\n");
    //printf("\nERRORS:\n");
    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
    printf("Kernel Finish\n");

    gpu::map_mutex.lock();
    //memcpy(gpu::host_map, devPtr, ses_settings.world_width * ses_settings.world_height * sizeof(int));
    cudaMemcpy(gpu::host_map, devPtr, sizeof(int)*ses_settings.world_width*ses_settings.world_height, cudaMemcpyDeviceToHost);

    gpu::map_mutex.unlock();
    //std::cout << "PRINTING WORLD" << std::endl;
    
    if(!verbose)
        return;

    for(int i = 0; i < ses_settings.world_width*ses_settings.world_height; i++)
    {
    	std::cout << gpu::host_map[i] << ' ';
    	if((i+1) % ses_settings.world_width == 0)
    		std::cout << '\n';
    }
}

void gpu::print_map_status()
{
    std::cout << "Current map status:" << std::endl;
    for(int i = 0; i < gpu::world_width; i++)
    {
    	for(int j = 0; j < gpu::world_height; j++)
    	{
    	    std::cout << gpu::host_map[i + j * gpu::world_width];
    	}
    	std::cout << '\n';
    }
}

void* gpu::allocate_device_mem(int bytes)
{
	void* address;
	cudaMalloc((void**)&address, bytes);
	return address;
}

int gpu::access_host_map(int x, int y)
{
    return gpu::host_map[x + y * gpu::world_width];
}

// THREAD-BASED IMPL //////////////////////////
//(int* temp_map, int* scratch_array, int thread_num, int scratch_array_size, int num_t    hreads)
//void search_room_thread_based(int* temp_map, coord* scratch_array, int scratch_array_size, int thread_num, int num_threads)

