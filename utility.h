#pragma once

#include <ctime>
#include <chrono>
#include <string>
#include <unordered_map>
#include <mutex>
#include <curand_kernel.h>

typedef std::chrono::high_resolution_clock::time_point time_point;

class utility
{
    static std::unordered_map<std::string, time_point> _time_points;

public:
	static bool global_verbose;
	static std::mutex print_mutex;

    static void start_clock(const std::string & s);
    static void print_clock(const std::string & s);
};

class session_settings
{
public:
    int world_width;
    int world_height;
    int room_width;
    int room_height;
    __device__ __host__ session_settings(int ww, int wh, int rw, int rh) : world_width(ww), world_height(wh), room_width(rw), room_height(rh) {}
};

class coord
{
public:
    int x;
    int y;
    
    __device__ __host__ coord(int x, int y): x(x), y(y) {} 
    __device__ __host__ coord(): x(-1), y(-1) {}
};

class ay_stack_shared
{
public:
    int size = 0;
    int max_size = 0;
    coord* data_array;    
    __device__ __host__ void init(coord* data_array, int max_size)
    {
    	this->data_array = data_array;
    	this->max_size = max_size;
    }

    __device__ __host__ void push(coord c)
    {
            //printf("\PUSHED TO STACK\n");

	if(size >= max_size)
	{
	    
	    printf("ERROR: STACK OUT OF MEMORY. current dize: %i, max size: %i", size, max_size);
	}
            //printf("\PUSHED TO STACK\n");

	data_array[size] = c;
	size ++;
    }

    __device__ __host__ coord top()
    {
	return data_array[size-1];
    }

    __device__ __host__ void pop()
    {
	size --;
    }

    __device__ __host__ bool empty()
    {
	return size == 0;
    }
    
    __device__ __host__ int count()
    {
	return size;
    }
    
    __device__ __host__ void to_string()
    {
	return;
	printf("STACK:\n");
	for(int i = 0; i < size; i++)
	    printf("(%i, %i)\n", data_array[i].x, data_array[i].y);
    }

    __device__ void shuffle(curandState & rand)
    {
    	for(int i = 0; i < size; i++)
    	{
            //float r = ((double)std::rand() / (RAND_MAX));
            float r = curand_uniform(&rand);
    	    int swap_index = r * (size - i) + i;
    	    coord temp = data_array[i];
    	    data_array[i] = data_array[swap_index];
    	    data_array[swap_index] = temp;
    	}
    }
};

class ay_stack
{
public:
    int size = 0;
    int max_size = 0;
    coord* data_array;
    __device__ __host__ ay_stack(coord* data_array, int max_size): data_array(data_array), max_size(max_size) {}
    __device__ __host__ void push(coord c)
    {
            //printf("\PUSHED TO STACK\n");

	if(size >= max_size)
	{
	    
	    printf("ERROR: STACK OUT OF MEMORY. current dize: %i, max size: %i", size, max_size);
	}
            //printf("\PUSHED TO STACK\n");

	data_array[size] = c;
	size ++;
    }

    __device__ __host__ coord top()
    {
	return data_array[size-1];
    }

    __device__ __host__ void pop()
    {
	size --;
    }

    __device__ __host__ bool empty()
    {
	return size == 0;
    }
    
    __device__ __host__ int count()
    {
	return size;
    }
    
    __device__ __host__ void to_string()
    {
	return;
	printf("STACK:\n");
	for(int i = 0; i < size; i++)
	    printf("(%i, %i)\n", data_array[i].x, data_array[i].y);
    }

    __device__ void shuffle(curandState & rand)
    {
    	for(int i = 0; i < size; i++)
    	{
            //float r = ((double)std::rand() / (RAND_MAX));
            float r = curand_uniform(&rand);
    	    int swap_index = r * (size - i) + i;
    	    coord temp = data_array[i];
    	    data_array[i] = data_array[swap_index];
    	    data_array[swap_index] = temp;
    	}
    }
};

class ay_stack_cpu
{
public:
    int size = 0;
    int max_size = 0;
    coord* data_array;
    ay_stack_cpu(coord* data_array, int max_size): data_array(data_array), max_size(max_size) {}
    void push(coord c)
    {
            //printf("\PUSHED TO STACK\n");

	if(size >= max_size)
	{
	    
	    printf("ERROR: STACK OUT OF MEMORY. current dize: %i, max size: %i", size, max_size);
	}
            //printf("\PUSHED TO STACK\n");

	data_array[size] = c;
	size ++;
    }

    coord top()
    {
	return data_array[size-1];
    }

    void pop()
    {
	size --;
    }

    bool empty()
    {
	return size == 0;
    }
    
    int count()
    {
	return size;
    }
    
    void to_string()
    {
	return;
	printf("STACK:\n");
	for(int i = 0; i < size; i++)
	    printf("(%i, %i)\n", data_array[i].x, data_array[i].y);
    }

    void shuffle(curandState & rand)
    {
    	for(int i = 0; i < size; i++)
    	{
            float r = ((double)std::rand() / (RAND_MAX));
    	    int swap_index = r * (size - i) + i;
    	    coord temp = data_array[i];
    	    data_array[i] = data_array[swap_index];
    	    data_array[swap_index] = temp;
    	}
    }
};