all: game

game: utility gpu cpu game.cu 
	/Developer/NVIDIA/CUDA-7.5/bin/nvcc --std=c++11 -O3 game.cu gpu.o cpu.o utility.o -I /usr/local/include/ -I /Library/Frameworks/SDL2.framework//Headers/ -I /Library/Frameworks/SDL2_image.framework/Headers/ -I /usr/local/cuda/include -L /usr/local/lib -L /Library/Frameworks/SDL2_image.framework/Versions/Current/ -lcudart -lsdl2 -lsdl2_image -o cuda_game

utility: utility.cu
	/Developer/NVIDIA/CUDA-7.5/bin/nvcc --std=c++11 -O3 utility.cu -c

gpu: gpu.cu
	/Developer/NVIDIA/CUDA-7.5/bin/nvcc --std=c++11 -O3 gpu.cu -c

cpu: cpu.cu
	/Developer/NVIDIA/CUDA-7.5/bin/nvcc --std=c++11 -O3 cpu.cu -c

clean:
	rm utility.o gpu.o cpu.o cuda_game

debug:
	sudo /Developer/NVIDIA/CUDA-7.5/bin/cuda-gdb cuda_game
