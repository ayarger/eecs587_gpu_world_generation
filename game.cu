/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission.*/

//Using SDL, SDL_image, standard IO, vectors, and strings
#include <SDL.h>
#include <SDL_image.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include <stdio.h>
#include <string> 
#include <vector>
#include <thread>
#include <cmath>
#include <iostream>
#include <map>

// TIMING
#include <ctime>
#include <chrono>

#include "gpu.h"
#include "cpu.h"
#include "utility.h"

//The dimensions of the level

//Screen dimension constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;
const int TILE_SIZE = 64;

bool up_key_down = false;
bool down_key_down = false;
bool right_key_down = false;
bool left_key_down = false;

#define OBJECT_TYPE_NONE -1;
#define OBJECT_TYPE_PLAYER 0
#define OBJECT_TYPE_ROCK 1
#define OBJECT_TYPE_WATER 2
#define OBJECT_TYPE_COIN 3

SDL_Rect camera;

bool operator<(SDL_Point const& p1, SDL_Point const& p2)
{
	if(p1.x < p2.x) return true;
	if(p1.x > p2.x) return false;
	if(p1.y < p2.y) return true;
	return false;
}


int get_world_width()
{
    return gpu::world_width * TILE_SIZE;
}

int get_world_height()
{
    return gpu::world_height * TILE_SIZE;
}

//Texture wrapper class
class LTexture
{
	public:
		//Initializes variables
		LTexture();

		//Deallocates memory
		~LTexture();

		//Loads image at specified path
		bool loadFromFile( std::string path );
		
		#ifdef _SDL_TTF_H
		//Creates image from font string
		bool loadFromRenderedText( std::string textureText, SDL_Color textColor );
		#endif

		//Deallocates texture
		void free();

		//Set color modulation
		void setColor( Uint8 red, Uint8 green, Uint8 blue );

		//Set blending
		void setBlendMode( SDL_BlendMode blending );

		//Set alpha modulation
		void setAlpha( Uint8 alpha );
		
		//Renders texture at given point
		void render( int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE );

		//Gets image dimensions
		int getWidth();
		int getHeight();

	private:
		//The actual hardware texture
		SDL_Texture* mTexture;

		//Image dimensions
		int mWidth;
		int mHeight;
};

//Scene textures
LTexture gRockTexture;
LTexture gDotTexture;
LTexture gBGTexture;
LTexture gCoinTexture;
LTexture gWaterTexture;

//The dot that will move around on the screen
class game_object
{
protected:
	SDL_Point _real_pos;
	SDL_Point _grid_pos;
	SDL_Point _dim;
	SDL_Point _vel;
	LTexture* _tex_ptr;
	int _object_type;

public:
	~game_object();
	game_object(const SDL_Point & initial_grid_position);

	SDL_Point get_position();
	bool should_cull();
	void render();
	const SDL_Point& get_real_position();
	const SDL_Point& get_grid_position();
	int get_object_type();
	virtual void cleanup(){};

	virtual void handleEvent(SDL_Event & e) {};
	virtual void update() {};
};

std::vector<game_object*> game_objects;

game_object::game_object(const SDL_Point & initial_grid_position) : _grid_pos(initial_grid_position) 
{
	_vel = {0, 0};
	_object_type = OBJECT_TYPE_NONE;
	_dim = {TILE_SIZE, TILE_SIZE};
	_real_pos = {initial_grid_position.x * TILE_SIZE, initial_grid_position.y * TILE_SIZE};
}

game_object::~game_object()
{
}

bool game_object::should_cull()
{
    if(abs(_real_pos.x-SCREEN_WIDTH * 0.5 - camera.x) > SCREEN_WIDTH * 0.6 || abs(_real_pos.y-SCREEN_HEIGHT*0.5 - camera.y) > SCREEN_HEIGHT * 0.6)
		return true;
    return false;
}

void game_object::render()
{
    _tex_ptr->render(_real_pos.x - camera.x, _real_pos.y - camera.y);
}

const SDL_Point& game_object::get_real_position()
{
	return _real_pos;
}
const SDL_Point& game_object::get_grid_position()
{
	return _grid_pos;
}

int game_object::get_object_type()
{
	return _object_type;
}

class Player : public game_object
{
	float movement_speed = 0.2;
	int can_make_move = false;
public:
	Player(const SDL_Point & initial_position);
	void handleEvent(SDL_Event & e);
	void update();
};

Player::Player(const SDL_Point & initial_position) : game_object(initial_position) 
{
	_object_type = OBJECT_TYPE_PLAYER;
	_tex_ptr = &gDotTexture;
}

void Player::handleEvent(SDL_Event & e)
{
	//If a key was pressed
	if( e.type == SDL_KEYDOWN && e.key.repeat == 0 )
    {
        switch( e.key.keysym.sym )
	    {
	        case SDLK_UP: 
	        	up_key_down = true;
	        break;
	        case SDLK_DOWN: 
	        	down_key_down = true;
	        break;
	        case SDLK_LEFT: 
	    		left_key_down = true;
	   	    break;
	        case SDLK_RIGHT: 
	        	right_key_down = true;
	        break;
	    }

        can_make_move = false;
    }
    //If a key was released
    else if( e.type == SDL_KEYUP && e.key.repeat == 0 )
    {
        switch( e.key.keysym.sym )
	    {
	        case SDLK_UP: 
	        	up_key_down = false;
	        break;
	        case SDLK_DOWN: 
	        	down_key_down = false;
	        break;
	        case SDLK_LEFT: 
	    		left_key_down = false;
	   	    break;
	        case SDLK_RIGHT: 
	        	right_key_down = false;
	        break;
	    }
    }
}

// Coins collected by the player
class Coin : public game_object
{
public:
	Coin(const SDL_Point & initial_position);
	void cleanup();
	~Coin();
};

std::map <SDL_Point, Coin*> points_to_coins;

Coin::Coin(const SDL_Point & initial_position) : game_object(initial_position) 
{
	points_to_coins[initial_position] = this;
	_object_type = OBJECT_TYPE_COIN;
	_tex_ptr = &gCoinTexture;
};

void Coin::cleanup()
{
	std::cout << "DESTROY COIN" << std::endl;
	//points_to_coins.erase(_grid_pos);
	for(int i = 0; i < game_objects.size(); i++)
	{
		if(game_objects[i] == this)
		{
			game_objects.erase(game_objects.begin() + i);
			break;
		}
	}

	//delete this;
}

Coin::~Coin()
{
	points_to_coins[_grid_pos] = NULL;
}


void Player::update()
{
	if(can_make_move)
	{
		can_make_move = false;

		SDL_Point new_grid_pos = _grid_pos;

		if(up_key_down)
			new_grid_pos = {_grid_pos.x, _grid_pos.y-1};
		else if(down_key_down)
			new_grid_pos = {_grid_pos.x, _grid_pos.y+1};
		else if(right_key_down)
			new_grid_pos = {_grid_pos.x+1, _grid_pos.y};
		else if(left_key_down)
			new_grid_pos = {_grid_pos.x-1, _grid_pos.y};

		//TODO: verify new_grid_pos isn't a wall or something.
		int cell_code = gpu::access_host_map(new_grid_pos.x, new_grid_pos.y);
		if(cell_code != OBJECT_TYPE_ROCK)
			_grid_pos = new_grid_pos;
		if(cell_code == OBJECT_TYPE_COIN)
		{
			Coin* coin_at_position = points_to_coins[new_grid_pos];
			coin_at_position->cleanup();
		}
	}

	int desired_x = _grid_pos.x * TILE_SIZE;
	int desired_y = _grid_pos.y * TILE_SIZE;

	int new_x = _real_pos.x + (desired_x - _real_pos.x) * movement_speed;
	int new_y = _real_pos.y + (desired_y - _real_pos.y) * movement_speed;

	this->_real_pos = {new_x, new_y};

	if(abs(new_x - desired_x) < 10 && abs(new_y - desired_y) < 10)
		can_make_move = true;
}

// Coins collected by the player
class Water : public game_object
{
public:
	Water(const SDL_Point & initial_position);
};

Water::Water(const SDL_Point & initial_position) : game_object(initial_position) 
{
	_object_type = OBJECT_TYPE_WATER;
	_tex_ptr = &gWaterTexture;
};

// Rocks block the player.
class Rock : public game_object
{
public:
	Rock(const SDL_Point & initial_position);
};

Rock::Rock(const SDL_Point & initial_position) : game_object(initial_position) 
{
	_object_type = OBJECT_TYPE_ROCK;
	_tex_ptr = &gRockTexture;
};

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

LTexture::LTexture()
{
	//Initialize
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

LTexture::~LTexture()
{
	//Deallocate
	free();
}

bool LTexture::loadFromFile( std::string path )
{
	//Get rid of preexisting texture
	free();

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Color key image
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}
		else
		{
			//Get image dimensions
			mWidth = loadedSurface->w;
			mHeight = loadedSurface->h;
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}

#ifdef _SDL_TTF_H
bool LTexture::loadFromRenderedText( std::string textureText, SDL_Color textColor )
{
	//Get rid of preexisting texture
	free();

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid( gFont, textureText.c_str(), textColor );
	if( textSurface != NULL )
	{
		//Create texture from surface pixels
        mTexture = SDL_CreateTextureFromSurface( gRenderer, textSurface );
		if( mTexture == NULL )
		{
			printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
		}
		else
		{
			//Get image dimensions
			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface( textSurface );
	}
	else
	{
		printf( "Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError() );
	}

	
	//Return success
	return mTexture != NULL;
}
#endif

void LTexture::free()
{
	//Free texture if it exists
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

void LTexture::setColor( Uint8 red, Uint8 green, Uint8 blue )
{
	//Modulate texture rgb
	SDL_SetTextureColorMod( mTexture, red, green, blue );
}

void LTexture::setBlendMode( SDL_BlendMode blending )
{
	//Set blending function
	SDL_SetTextureBlendMode( mTexture, blending );
}
		
void LTexture::setAlpha( Uint8 alpha )
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod( mTexture, alpha );
}

void LTexture::render( int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip )
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };

	//Set clip rendering dimensions
	if( clip != NULL )
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx( gRenderer, mTexture, clip, &renderQuad, angle, center, flip );
}

int LTexture::getWidth()
{
	return mWidth;
}

int LTexture::getHeight()
{
	return mHeight;
}

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create vsynced renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Load dot texture
	if( !gDotTexture.loadFromFile( "images/dot.bmp" ) )
	{
		printf( "Failed to load dot texture!\n" );
		success = false;
	}

	//Load background texture
	if( !gBGTexture.loadFromFile( "images/bg.png" ) )
	{
		printf( "Failed to load background texture!\n" );
		success = false;
	}

	// Load rock texture
	
	if( !gRockTexture.loadFromFile( "images/rock.png" ) )
	{
	    printf("Failed to load rock texture!\n");
	    success = false;
	}

	if( !gCoinTexture.loadFromFile( "images/coin.png" ) )
	{
	    printf("Failed to load coin texture!\n");
	    success = false;
	}

	if( !gWaterTexture.loadFromFile( "images/water.png" ) )
	{
	    printf("Failed to load water texture!\n");
	    success = false;
	}

	return success;
}

void close()
{
	//Free loaded images
	gDotTexture.free();
	gBGTexture.free();
	gRockTexture.free();
	gCoinTexture.free();
	gWaterTexture.free();

	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

void refresh_map()
{
    gpu::map_mutex.lock();
    for(int x = 0; x < gpu::world_width; x++)
    {
        for(int y = 0; y < gpu::world_height; y++)
        {
           SDL_Point new_object_grid_pos = {x, y};
           int cell_code = gpu::access_host_map(x, y);
		   if(cell_code == OBJECT_TYPE_ROCK)
		   {
				Rock* new_rock = new Rock(new_object_grid_pos);
				game_objects.push_back(new_rock);
		   }

		   else if(cell_code == OBJECT_TYPE_COIN)
		   {
		   		Coin* new_coin = new Coin(new_object_grid_pos);
				game_objects.push_back(new_coin);
		   }

		   else if(cell_code == OBJECT_TYPE_WATER)
		   {
		   		Water* new_water = new Water(new_object_grid_pos);
				game_objects.push_back(new_water);
		   }
        }
    }
    gpu::map_mutex.unlock();
}

void compute_map()
{
	// Test GPU
	// This is a dummy run to ensure the gpu gets loaded.
	// Including this run in measurements would skew results.
	gpu::exp_init(false, true);
	for(int i = 0; i < gpu::num_runs; i++)
	{
    	utility::start_clock("gpu" + std::to_string(i));
    	gpu::exp_init(utility::global_verbose, false);
    	utility::print_clock("gpu" + std::to_string(i));  
	}

	// Test CPU
	if(gpu::test_both_gpu_and_cpu)
	{
		for(int i = 0; i < gpu::num_runs; i++)
		{
	    	utility::start_clock("cpu" + std::to_string(i));
	    	cpu::init(utility::global_verbose);
	    	utility::print_clock("cpu" + std::to_string(i));  
		}
 	}
}

void process_input(int argc, char* args[])
{
	if(argc == 9)
	{
		gpu::world_width = atoi(args[1]);
		gpu::world_height = atoi(args[2]);
		gpu::room_width = atoi(args[3]);
		gpu::room_height = atoi(args[4]);
		if(!strcmp(args[5],"true") || !strcmp(args[5], "True") || !strcmp(args[5],"t"))
			gpu::test_both_gpu_and_cpu = true;
		gpu::num_runs = atoi(args[6]);
		if(!strcmp(args[7],"true") || !strcmp(args[7], "True") || !strcmp(args[7],"t"))
			gpu::dry_run = true;

		if(!strcmp(args[8],"true") || !strcmp(args[8], "True") || !strcmp(args[8],"t"))
			utility::global_verbose = true;
	}
}

// usage: ./cuda_game <world_width> world_height room_width room_height both_gpu_cpu num_runs dry_run global_verbose
int main( int argc, char* args[] )
{
	process_input(argc, args);
	srand(time(NULL));
	compute_map();

	if(gpu::dry_run)
		return;
	refresh_map();

	//std::thread background_gpu_thread(compute_map);
	//gpu::print_map_status();	

	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			//The dot that will be moving around on the screen
			SDL_Point starting_player_pos = {2, 2};
			Player player(starting_player_pos);

			//The camera area
			camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };

			//While application is running
			while( !quit )
			{
				//Handle events on queue
				while( SDL_PollEvent( &e ) != 0 )
				{
					//User requests quit
					if( e.type == SDL_QUIT )
					{
						quit = true;
					}

					player.handleEvent(e);
				}
				
				//Clear screen
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( gRenderer );

				//Render background
				gBGTexture.render( 0, 0, &camera );	

				float desired_x =  ( player.get_real_position().x + TILE_SIZE / 2 ) - SCREEN_WIDTH / 2;
				float desired_y =  ( player.get_real_position().y + TILE_SIZE / 2 ) - SCREEN_HEIGHT / 2;
				camera.x += (desired_x - camera.x) * 0.1;
				camera.y += (desired_y - camera.y) * 0.1;

				//Keep the camera in bounds
				if( camera.x < 0 )
				{ 
					camera.x = 0;
				}
				if( camera.y < 0 )
				{
					camera.y = 0;
				}
				if( camera.x > get_world_width() - camera.w )
				{
					camera.x = get_world_width() - camera.w;
				}
				if( camera.y > get_world_height() - camera.h )
				{
					camera.y = get_world_height() - camera.h;
				}

				//Render objects 
				player.update();

				for(int i = 0; i < game_objects.size(); i++)
				{
					game_objects[i]->update();

				    if(!game_objects[i]->should_cull())
						game_objects[i]->render();
				}

				player.render();

				//Update screen
				SDL_RenderPresent( gRenderer );
			}
		}
	}

	//Free resources and close SDL
	close();

	return 0;
}
