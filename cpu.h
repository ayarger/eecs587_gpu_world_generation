#pragma once

#include "utility.h"

class cpu
{
public:
    static int num_visited_adjacent_spaces(const int* devPtr, const coord & co, const coord & origin, const session_settings & ses_settings);
    static bool add_potential_coords(const int* devPtr, ay_stack_cpu & stack, const coord & co, const coord & origin, curandState & rand, const session_settings & ses_settings);
    static void search_room(int* devPtr, coord* scratch_array, int scratch_array_size, coord origin, curandState & rand, const session_settings & ses_settings);
    static void clear_room(int * devPtr, const session_settings & ses_settings, coord origin);
    static void search_room_thread_based(int* temp_map, coord* scratch_array, int scratch_array_size, int thread_num, int num_threads, session_settings ses_settings);
    static void init(bool verbose);
};