#include "cpu.h"

#include <curand_kernel.h>

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <time.h>
#include <vector>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <thread>
#include "utility.h"
#include "gpu.h"

int cpu::num_visited_adjacent_spaces(const int* devPtr, const coord & co, const coord & origin, const session_settings & ses_settings)
{
    int num_visited_adjacent_spaces = 0;
    
    //NORTH
    if(co.y > origin.y && devPtr[co.x + (co.y-1)*ses_settings.world_width] == 0)
    {
     num_visited_adjacent_spaces ++;
    }

    //EAST
    if(co.x < origin.x + ses_settings.room_width-1 && devPtr[(co.x+1) + co.y*ses_settings.world_width] == 0)
    {
     num_visited_adjacent_spaces ++;
    }

    //SOUTH
    if(co.y < origin.y + ses_settings.room_height-1 && devPtr[co.x + (co.y+1)*ses_settings.world_width] == 0)
    {
     num_visited_adjacent_spaces ++;
    }

    //WEST
    if(co.x > origin.x && devPtr[(co.x-1) + co.y*ses_settings.world_width] == 0)
    {
     num_visited_adjacent_spaces ++;
    }
    return num_visited_adjacent_spaces;
}

bool cpu::add_potential_coords(const int* devPtr, ay_stack_cpu & stack, const coord & co, const coord & origin, curandState & rand, const session_settings & ses_settings)
{
    int adjacency_limit = 1;

    //NORTH
    if(co.y > origin.y)
    {
        if(devPtr[co.x + (co.y-1)*ses_settings.world_width] == 1)
        {
            if(num_visited_adjacent_spaces(devPtr, coord(co.x, co.y-1), origin, ses_settings) <= adjacency_limit)
            stack.push(coord(co.x, co.y-1));
        }   
    }
    //EAST
    if(co.x < origin.x + ses_settings.room_width-1)
    {
        if(devPtr[(co.x+1) + co.y*ses_settings.world_width] == 1)
        {
            if(num_visited_adjacent_spaces(devPtr, coord(co.x+1, co.y), origin, ses_settings) <= adjacency_limit)
            stack.push(coord(co.x+1, co.y));
        }
    }
    //SOUTH
    if(co.y < origin.y + ses_settings.room_height-1)
    {
        if(devPtr[co.x + (co.y+1)*ses_settings.world_width] == 1)
        {
            if(num_visited_adjacent_spaces(devPtr, coord(co.x, co.y+1), origin, ses_settings) <= adjacency_limit)
            stack.push(coord(co.x, co.y+1));
        }
    }
    //WEST TODO:.
    if(co.x > origin.x)
    {
        if(devPtr[(co.x-1) + co.y*ses_settings.world_width] == 1)
        {
            if(num_visited_adjacent_spaces(devPtr, coord(co.x-1, co.y), origin, ses_settings) <= adjacency_limit)
            stack.push(coord(co.x-1, co.y));
        }
    }

    stack.shuffle(rand);

    return false;
}


void cpu::search_room(int* devPtr, coord* scratch_array, int scratch_array_size, coord origin, curandState & rand, const session_settings & ses_settings)
{   
                    //printf("\PRE PUSH TO STACK2\n");

    ay_stack_cpu stack(scratch_array, scratch_array_size);
    // starting point.
                //printf("\PRE PUSH TO STACK\n");

    stack.push(origin);

    int num = 0;
    while(!stack.empty())
    {
        //printf("\nWITHIN STACK\n");

        // Grab next coord.
        coord current_coord = stack.top();
        stack.pop();
        
        if(num_visited_adjacent_spaces(devPtr, coord(current_coord.x, current_coord.y), origin, ses_settings) > 1)
            continue;

        devPtr[current_coord.x + current_coord.y*ses_settings.world_width] = 0;
        //printf("\nSET TO 0!!!!!!!!!!!!!!!!!!!\n");

        //float r = curand_uniform(&rand);
        //float r = 0.5f;
        
        //if(r > 0.95)
        //    clear_adjacent_spaces(devPtr, current_coord, origin);         

        num ++;
            
        cpu::add_potential_coords(devPtr, stack, current_coord, origin, rand, ses_settings);
        
        stack.to_string();
    }

    devPtr[origin.x + origin.y * ses_settings.world_width] = 3;
}

void cpu::clear_room(int * devPtr, const session_settings & ses_settings, coord origin)
{
    for(int x = origin.x; x < origin.x + ses_settings.room_width; x++)
    {
        for(int y = origin.y; y < origin.y + ses_settings.room_height; y++)
        {

            devPtr[x + y * ses_settings.world_width] = 1;
        }
    }
}


void cpu::search_room_thread_based(int* temp_map, coord* scratch_array, int scratch_array_size, int thread_num, int num_threads, session_settings ses_settings)
{
    int num_regions_wide = ses_settings.world_width / ses_settings.room_width;
    int num_regions_tall = ses_settings.world_height / ses_settings.room_height;

    coord origin((thread_num % num_regions_wide) * ses_settings.room_width, (thread_num / num_regions_wide) * ses_settings.room_height);

    // Prepare room.
    clear_room(temp_map, ses_settings, origin);

    // Search room.
    int scratch_space_per_thread = scratch_array_size / num_threads;
    curandState rand_thread_state;
    search_room(temp_map, scratch_array, scratch_space_per_thread, origin, rand_thread_state, ses_settings);
    utility::print_mutex.lock();
    //std::cout << "thread " << thread_num << " finished." << std::endl;
    utility::print_mutex.unlock();
}

void cpu::init(bool verbose)
{
    session_settings ses_settings(gpu::world_width, gpu::world_height, gpu::room_width, gpu::room_height);
    srand(time(NULL));
    int num_threads = (ses_settings.world_width / ses_settings.room_width) * (ses_settings.world_height / ses_settings.room_height);
    
    gpu::host_map = new int[ses_settings.world_width * ses_settings.world_height]();
    int* temp_map = (int*)malloc(sizeof(int) * ses_settings.world_width*ses_settings.world_height);

    int stack_space_required = ses_settings.room_width * ses_settings.room_height * num_threads * 1.0f; // RUN CUDA DEBUGGER TO DISCOVER WHY CRASH HAPPENS WHEN WE DO WORLD:256, ROOM:4.
    coord* scratch_array = (coord*)malloc(sizeof(coord) * stack_space_required);

    printf("num_threads:%d\n", num_threads);
    for(int i = 0; i < num_threads; i++)
    {
        std::thread new_thread(search_room_thread_based, temp_map, scratch_array, stack_space_required, i, num_threads, ses_settings);
        new_thread.join();
    }
    
    memcpy(gpu::host_map, temp_map, sizeof(int) * ses_settings.world_width*ses_settings.world_height);

    if(!verbose)
        return;
    std::cout << "PRINTING WORLD" << std::endl;
    for(int i = 0; i < ses_settings.world_width*ses_settings.world_height; i++)
    {
        std::cout << temp_map[i] << ' ';
        if((i+1) % ses_settings.world_width == 0)
            std::cout << '\n';
    }
}
